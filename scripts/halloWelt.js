'use strict';

// exercise 1
function showMessage() {
    var var2 = 2;
    const CON1 = 500;
    let array1 = ['Hallo', 'Welt'];
    console.log('Hallo Welt');
    console.info(var2);
    console.error(CON1);

    for (let i = 0; i < array1.length; i++) {
        console.log(array1[i]);
    }
}

// exercise 2

// Object
let item1 = {
    name: 'item1',
    price: 10,
    color: 'blue',
    showData: function() {
        console.log('Name: ' + this.name + '\nPrice: ' + this.price);
    }
}

// exercise 3
let sourceList = document.getElementById('source');
let targetList = document.getElementById('target');
let buttonLeft = document.getElementById('move-left');
let buttonRight = document.getElementById('move-right');

buttonRight.addEventListener('click', function () {
    while (sourceList.hasChildNodes()) {
        let item = sourceList.removeChild(sourceList.firstChild);
        targetList.appendChild(item);
    }
})

buttonLeft.addEventListener('click', function () {
    while (targetList.hasChildNodes()) {
        let item = targetList.removeChild(targetList.firstChild);
        sourceList.appendChild(item);
    }
})

// exercise 3

document.addEventListener('DOMContentLoaded', function () {
    let button = document.getElementById('button-calculate-sum');
    button.addEventListener('click', calculateSum);
})

function calculateSum() {
    let x = parseInt(document.getElementById('field1').value);
    let y = parseInt(document.getElementById('field2').value);
    let result = x + y;
    showResult(result);
}

function showResult(result) {
    let resultField = document.getElementById('result');
    resultField.value = result;
}
// execution
showMessage()
item1.showData()